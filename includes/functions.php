<?php

function wallet_app_cryptocurency_setting_currency_list()
{
    return array(
      'BTC',
      'ETH',
      'USD',
      'AED',
      'ARS',
      'AUD',
      'BRL',
      'CAD',
      'CHF',
      'CLP',
      'CNY',
      'CZK',
      'DKK',
      'EUR',
      'GBP',
      'HKD',
      'HUF',
      'IDR',
      'ILS',
      'INR',
      'JPY',
      'KRW',
      'KWD',
      'LKR',
      'MXN',
      'MYR',
      'NOK',
      'NZD',
      'PHP',
      'PKR',
      'PLN',
      'RUB',
      'SAR',
      'SEK',
      'SGD',
      'THB',
      'TRY',
      'TWD',
      'ZAR'
    );
}

function wallet_app_cryptocurency_setting_default_values()
{
    $data = array();

    $data['title'] = 'Currency by Wallet.app';
    $data['type'] = 'marquee';
    $data['currency'] = 'USD';
    $data['background'] = '';
    $data['width'] = '';
    $data['coins'] = wallet_app_cryptocurency_default_coins();
    $data['ssl'] = 0;

    return $data;
}

function wallet_app_cryptocurency_setting_type_list()
{
    $data = array();

    $data['marquee']['title'] = 'Marquee widget';
    $data['marquee']['tag_name'] = 'coin-price-marquee-widget';
    $data['marquee']['url_type'] = 'coin-price-marquee-widget';
    $data['list']['title'] = 'List widget';
    $data['list']['tag_name'] = 'twins-coin-list-widget';
    $data['list']['url_type'] = 'coin-list-widget';

    return $data;
}

function wallet_app_cryptocurency_default_coins()
{
    $data = array();

    $data['bitcoin'] = 'Bitcoin (BTC)';
    $data['ethereum'] = 'Ethereum (ETH)';
    $data['ripple'] = 'XRP (XRP)';
    $data['eos'] = 'EOS (EOS)';
    $data['litecoin'] = 'Litecoin (LTC)';
    $data['bitcoin-cash'] = 'Bitcoin Cash (BCH)';
    $data['nem'] = 'NEM (XEM)';

    return $data;
}

function wallet_app_cryptocurency_widget_code($params)
{

    $widget_types = wallet_app_cryptocurency_setting_type_list();
    $tag_name = $widget_types[$params['type']]['tag_name'];
    $url_type = $widget_types[$params['type']]['url_type'];

    $width = (array_key_exists('width', $params) ? ('width="'. $params['width'] .'"') : '');
    $background = (array_key_exists('background', $params) ? ('background="'. $params['background'] .'"') : '');

    $http_type = ($params['ssl'] == 1 ? 'https' : 'http');
    $url = $http_type .'://104.248.28.171/assets/widgets/'. $url_type .'/widget.js';

    $output = '<script src="'.$url.'"></script><'.$tag_name.' coin-ids="'. implode(",", $params['coins']) .'" currency="'. strtolower($params['currency']) .'" ' . $background . ' locale="en" '. $width .'/>';

    return $output;
}

function wallet_app_cryptocurency_setting_render_coin_list($values, $type)
{
    $output_string = '';
    foreach ($values as $k => $v) {
        $output_string .= '<div class="wallet-app-selected-coin"><input type="hidden" name="'.$type.'['.$k.']" value="'.$v.'">'.$v.'<div class="wallet-app-colin-list-action"><div class="dashicons dashicons-arrow-up-alt2" onclick="moveCoinElement(this, \'up\')"></div><div class="dashicons dashicons-arrow-down-alt2" onclick="moveCoinElement(this, \'down\')"></div><div class="dashicons dashicons-no" onclick="removeCoinElement(this)"></div></div></div>'.PHP_EOL;
    }

    echo $output_string;
}

function wallet_app_cryptocurency_setting_render_select_options($code, $arr)
{
    $output_string = '';
    foreach ($arr as $k => $v) {
        $output_string .= '<option value="'.$k.'"'.(($code == $k) ? ' selected' : '').'>'.$v.'</option>'.PHP_EOL;
    }

    echo $output_string;
}
