<?php
/**
    Plugin Name: Cryptocurrency Widgets by Wallet.app
    Description: Cryptocurrency Widgets WordPress plugin displays current prices of over crypto coins - bitcoin, ethereum, ripple and +4000 more. Totally free, no ads.
    Version: 0.1
    Author: Wallet.app
*/

/*
    Init widget
*/
require_once 'includes/functions.php';

add_action('widgets_init', function () {
    register_widget('wallet_app_cryptocurency_widget');
});

class wallet_app_cryptocurency_widget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            'wallet_app_cryptocurency_widget',
            esc_html__('Cryptocurrency Widgets by Wallet.app', 'wallet_app_cryptocurency_widget'),
            array(
                'description' => esc_html__('Cryptocurrency Widgets WordPress plugin displays current prices of over crypto coins - bitcoin, ethereum, ripple and +4000 more. Totally free, no ads.', 'wallet_app_cryptocurency_widget'),
            )
        );
    }

    /* Update the widget settings. */
    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;

        $instance['title'] = sanitize_text_field($new_instance['title']);
        $instance['currency'] = sanitize_text_field($new_instance['currency']);
        $instance['type'] = sanitize_text_field($new_instance['type']);
        $instance['width'] = sanitize_text_field($new_instance['width']);
        $instance['background'] = sanitize_text_field($new_instance['background']);
        $instance['ssl'] = sanitize_text_field($new_instance['ssl']);
        $instance['coins'] = array();
        foreach ( $new_instance['coins'] as $k => $v )
        {
            $instance['coins'][sanitize_text_field($k)] = sanitize_text_field($v);
        }
        if (empty($instance['coins'])) {
          $instance['error'] = "Coin list should be filled.";
        }

        return $instance;
    }

    public function form($instance)
    {

      // Register style
      wp_register_style('wallet-app-cryptocurrency', plugin_dir_url(__FILE__).'assets/wallet-app-cruptocurency.css');
      wp_enqueue_style('wallet-app-cryptocurrency', plugin_dir_url(__FILE__).'assets/wallet-app-cruptocurency.css');

      wp_register_script('wallet-app-cryptocurrency', plugin_dir_url(__FILE__).'assets/wallet-app-cruptocurency.js');
      wp_enqueue_script('wallet-app-cryptocurrency', plugin_dir_url(__FILE__).'assets/wallet-app-cruptocurency.js');

        $defaults = wallet_app_cryptocurency_setting_default_values();

        if (empty($instance)) {
            $instance = $defaults;
        }

        $currency_collection = wallet_app_cryptocurency_setting_currency_list();
        $currency_list = array();
        foreach ($currency_collection as $value) {
            $currency_list[$value] = $value;
        }

        $type_collection = wallet_app_cryptocurency_setting_type_list();
        $type_list = array();
        foreach ($type_collection as $key => $value) {
            $type_list[$key] = $value['title'];
        }

        $currency = sanitize_text_field($instance['currency']);
        $title = sanitize_text_field($instance['title']);
        $type = sanitize_text_field($instance['type']);
        $coins = $instance['coins'];
        $width = sanitize_text_field($instance['width']);
        $background = sanitize_text_field($instance['background']);
	      $ssl = sanitize_text_field($instance['ssl']);

        echo '<p><label for="',$this->get_field_id('title'),'">Title:',
             '<input id="wallet-app-title" type="text" name="',$this->get_field_name('title'),'" value="',$title,'" style="width:100%"></label></p>';

        echo '<p><label for="',$this->get_field_id('currency'),'">Currency:',
             '<select id="',$this->get_field_id('currency'),'" name="',$this->get_field_name('currency'),'" style="width:100%">',
             wallet_app_cryptocurency_setting_render_select_options($currency, $currency_list),
             '</select></label></p>';

       echo '<p><label>Added coins:',
            '<div class="wallet-app-selected-coins" data-coin-type="',$this->get_field_name('coins'),'">',
            wallet_app_cryptocurency_setting_render_coin_list($coins, $this->get_field_name('coins')),
            '</div></label></p>';

        echo '<p><label for="wallet-app-search-seeker">Search for coins to add:',
             '<div class="wallet-app-search-bar"><input type="text" name="wallet-app-search-seeker" oninput="filterCoinData(this)" class="wallet-app-search-seeker"><div class="wallet-app-search-content"></div></div>',
             '</label></p>';

        echo '<p><label for="',$this->get_field_id('type'),'">Widget type:',
             '<select id="',$this->get_field_id('type'),'" name="',$this->get_field_name('type'),'" style="width:100%">',
             wallet_app_cryptocurency_setting_render_select_options($type, $type_list),
             '</select></label></p>';

        echo '<p><label for="',$this->get_field_id('width'),'">Width:',
             '<input id="',$this->get_field_id('width'),'" type="text" name="',$this->get_field_name('width'),'" value="',$width,'" placeholder="100% or 200px" style="width:100%"></label></p>';

        echo '<p><label for="',$this->get_field_id('background'),'">Background color:',
             '<input id="',$this->get_field_id('background'),'" type="text" name="',$this->get_field_name('background'),'" value="',$background,'" placeholder="#ffffff or rgba(255,255,255,.6) or yellow" style="width:100%"></label></p>';

  	    echo '<p><label for="',$this->get_field_id('ssl'),'">',
  	    '<input type="checkbox" ',checked($ssl, 2),' id="',$this->get_field_id('ssl'),'" name="',$this->get_field_name('ssl'),'" value="2">SSL</label></p>';

    }

    /*
        Output widget
    */
    public function widget($args, $instance)
    {
        extract($args);

        $defaults = wallet_app_cryptocurency_setting_default_values();

        if (empty($instance)) {
            $instance = $defaults;
        }

        $currency = sanitize_text_field($instance['currency']);
        $type = sanitize_text_field($instance['type']);
        $title = sanitize_text_field($instance['title']);
        $width = sanitize_text_field($instance['width']);
        $background = sanitize_text_field($instance['background']);
	      $ssl = sanitize_text_field($instance['ssl']);

        echo $args['before_widget'];

        // Title
        echo $args['before_title'].$title.$args['after_title'];

        // Output
        echo wallet_app_cryptocurency_widget_code(array(
            'currency' => $currency,
            'type' => $type,
            'ssl' => $ssl,
            'width' => $width,
            'background' => $background,
            'coins' => array_keys($instance['coins'])
        ));

        echo $args['after_widget'];
    }
}
